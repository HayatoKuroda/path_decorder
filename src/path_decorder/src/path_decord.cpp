#include <ros/ros.h>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <path_decorder/TrajectoryPath.h>
#include "std_msgs/String.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/optional.hpp>

using namespace boost::property_tree;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "path_decorder");
	ros::NodeHandle pn("~");
	std::string msg_chatter;
	std::string name_path;
	std::string map_id;
	std::string owner_id;
	ros::Rate loop_rate(5);
	namespace ser = ros::serialization;
	boost::shared_array<uint8_t> buffer;
	path_decorder::TrajectoryPath pose_vel_array;	
	
	while(ros::ok())
	{
		//get file place form launch file
		if(pn.getParam("data_place", msg_chatter)){
			//for file operation
			std::ifstream ifs;
			std::ofstream ofs;
  			ifs.open(msg_chatter, std::ios::binary);
			ofs.open("/home/black/catkin_ws/output.txt", std::ios::out);

			//check the size of reading data
			ifs.seekg(0, std::ios::end);
			long long int size = ifs.tellg();
			ifs.seekg(0,ifs.beg);
			buffer.reset(new uint8_t[size]); 
			//Output the read data to char type
			ifs.read((char*)buffer.get(), size); 

			ros::serialization::IStream stream(buffer.get(), size);
			ros::serialization::deserialize(stream, pose_vel_array);

			//パス確認(csv形式)
			int size_path = pose_vel_array.header.seq;
			// for (int i = 0; i < size_path; i++) {
			// 	ofs << pose_vel_array.poses[i].pose.position.x << ", "<< pose_vel_array.poses[i].pose.position.y << "\n ";
			// }

			//求められたjsonにて出力
			pn.getParam("name_path", name_path);
			pn.getParam("map_id", map_id);
			pn.getParam("owner_id", owner_id);
			ptree pt;

			pt.put("name", name_path);
			pt.put("map_id", map_id);
			ptree child;
			{
				for (int i = 0; i < size_path; i++) {
					ptree info;
					info.put("x", pose_vel_array.poses[i].pose.position.x);
					info.put("y", pose_vel_array.poses[i].pose.position.y);
					child.push_back(std::make_pair("", info));
				}
			}
			pt.add_child("point", child);
			pt.put("owner_id", owner_id);
			write_json(ofs, pt);			
			
			while (!ifs.eof())
			{

			}						
		} else {
			ROS_ERROR_STREAM("Failed to get data_place at" << ros::this_node::getName());
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}