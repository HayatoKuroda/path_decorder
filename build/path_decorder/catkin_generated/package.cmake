set(_CATKIN_CURRENT_PACKAGE "path_decorder")
set(path_decorder_VERSION "0.1.0")
set(path_decorder_MAINTAINER "Atsushi Watanabe <atsushi.w@ieee.org>")
set(path_decorder_PACKAGE_FORMAT "1")
set(path_decorder_BUILD_DEPENDS "message_generation" "roscpp" "rospy" "std_msgs" "nav_msgs" "geometry_msgs" "tf" "interactive_markers" "TrajectoryMessage")
set(path_decorder_BUILD_EXPORT_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs" "nav_msgs" "geometry_msgs" "tf" "interactive_markers" "TrajectoryMessage")
set(path_decorder_BUILDTOOL_DEPENDS "catkin")
set(path_decorder_BUILDTOOL_EXPORT_DEPENDS )
set(path_decorder_EXEC_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs" "nav_msgs" "geometry_msgs" "tf" "interactive_markers" "TrajectoryMessage")
set(path_decorder_RUN_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs" "nav_msgs" "geometry_msgs" "tf" "interactive_markers" "TrajectoryMessage")
set(path_decorder_TEST_DEPENDS )
set(path_decorder_DOC_DEPENDS )
set(path_decorder_URL_WEBSITE "")
set(path_decorder_URL_BUGTRACKER "")
set(path_decorder_URL_REPOSITORY "")
set(path_decorder_DEPRECATED "")