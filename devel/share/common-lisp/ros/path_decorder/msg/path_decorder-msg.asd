
(cl:in-package :asdf)

(defsystem "path_decorder-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "TrajectoryMessage" :depends-on ("_package_TrajectoryMessage"))
    (:file "_package_TrajectoryMessage" :depends-on ("_package"))
    (:file "TrajectoryPath" :depends-on ("_package_TrajectoryPath"))
    (:file "_package_TrajectoryPath" :depends-on ("_package"))
  ))